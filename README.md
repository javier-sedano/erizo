Reference architecture of public web application with mobile-first responsive design based on Java/Spring and Vue with CI in Gitlab-CI and CD to Google Compute Engine.

Work in progress

Master branch:
[![pipeline status](https://gitlab.com/javier-sedano/erizo/badges/master/pipeline.svg)](https://gitlab.com/javier-sedano/erizo/commits/master)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.erizo&metric=alert_status)](https://sonarcloud.io/dashboard?id=com.odroid.erizo)
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.erizo&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=com.odroid.erizo)
<details>
<summary>Quality details</summary>

[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.erizo&metric=bugs)](https://sonarcloud.io/dashboard?id=com.odroid.erizo)
[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.erizo&metric=code_smells)](https://sonarcloud.io/dashboard?id=com.odroid.erizo)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.erizo&metric=coverage)](https://sonarcloud.io/dashboard?id=com.odroid.erizo)
[![Duplicated Lines (%)](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.erizo&metric=duplicated_lines_density)](https://sonarcloud.io/dashboard?id=com.odroid.erizo)
[![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.erizo&metric=ncloc)](https://sonarcloud.io/dashboard?id=com.odroid.erizo)
[![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.erizo&metric=reliability_rating)](https://sonarcloud.io/dashboard?id=com.odroid.erizo)
[![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.erizo&metric=security_rating)](https://sonarcloud.io/dashboard?id=com.odroid.erizo)
[![Technical Debt](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.erizo&metric=sqale_index)](https://sonarcloud.io/dashboard?id=com.odroid.erizo)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.erizo&metric=vulnerabilities)](https://sonarcloud.io/dashboard?id=com.odroid.erizo)

[Sonar analysis](https://sonarcloud.io/dashboard?id=com.odroid.erizo)
</details>

Keywords: java, spring, springframework, sonarqube, vue, vscode, devcontainer, docker, w3css, fontawesome, giphy

Instructions:
* Suggested environment:
  * Visual Studio Code. Plugins:
    * Docker
    * Remote - Containers
  * Docker. Docker Desktop for Windows is tested
* Open VSCode and use `F1` --> `Remote containers: Clone Repository in Container Volume...` --> Paste Git clone URL --> `Create a new volume...` (or choose an existing one) --> Accept default name
  * Remote container will be detected and automatically used.
    * It may fail the first time with `The languaje Support for Java server crashed 5 times in the last 3 minutes. The server will not be restarted`, because extensions are installed in a wrong order (especifically, Java Languaje Server will start with Lombok options, but Lombok will not be available yet). How to solve it? One of the two following:
      * An offer to `Switch to Standard Mode` will be offered in the `Java Projects` panel of the Explorer view. Do it.
      * An offer like `The workspace contains Java projects. Would you like to import them?` will be shown in the lower-right notification. Say `Yes`.
      * Either way, the Java Languaje Server will be restarted in Standard Mode and it works now.
        * It may happen again if the container is rebuilt.
    * If the Java project is not automatically detected and imported, forcing the download of gradle (for example, with `./gradlew bootRun`) may help.
  * Run in VSCode (the order is relevant):
    * In the Run panel, the launch configuration "Debug (Launch)-ErizoApplication<erizo>" (will also be offered in the lower status panel), to run the backend.
    * In the TaskExplorer panel of the Explorer view or the TaskExplorer panel, the task vscode/frontBuildWatch, to watch-build the frontent.
  * Browse http://localhost:4007/erizo/ (or http://localhost:8080/erizo/ if not dockerized).
  * GradleTasks will show the available tasks.
  * To quicklyt try it, run `./gradlew bootRun`.

Useful links:

* https://jasmine.github.io/
* https://docs.sonarqube.org/latest/
* https://vuejs.org/
* https://cli.vuejs.org/
* https://router.vuejs.org/
* https://jestjs.io/
* https://www.w3schools.com/w3css/
* https://fontawesome.com/
* https://github.com/axios/axios
* https://github.com/kentcdodds/cross-env#readme
* https://developers.giphy.com/docs/api/endpoint/#random
* https://junit.org/junit5/docs/current/user-guide/
