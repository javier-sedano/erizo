package com.odroid.erizo.service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.Duration;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteWatchdog;
import org.apache.commons.exec.Executor;
import org.apache.commons.exec.PumpStreamHandler;
import org.springframework.stereotype.Service;

@Service
public class ShellService {
  public String exec(String command, long timeout) {
    try {
      var commandLine = CommandLine.parse(command);
      Executor executor = DefaultExecutor.builder().get();
      var executeWatchdog = ExecuteWatchdog.builder().setTimeout(Duration.ofMillis(timeout)).get();
      executor.setWatchdog(executeWatchdog);
      var stdout = new ByteArrayOutputStream();
      var psh = new PumpStreamHandler(stdout);
      executor.setStreamHandler(psh);
      executor.execute(commandLine);
      return stdout.toString();
    } catch (IOException e) {
      throw new IllegalArgumentException(e);
    }
  }
}
