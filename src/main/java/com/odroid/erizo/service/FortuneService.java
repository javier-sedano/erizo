package com.odroid.erizo.service;

import com.odroid.erizo.AppException;

import org.springframework.stereotype.Service;

@Service
public class FortuneService {
  private static final int MAX_FORTUNES = 5;
  private static final String FORTUNE = "/usr/games/fortune";
  private static final long EXECUTE_TIMEOUT = 60000;

  private ShellService shellService;

  public FortuneService(ShellService shellService) {
    this.shellService = shellService;
  }

  public String[] getFortunes(int n) {
    if (n <= 0) {
      throw new AppException(n + " is too small");
    }
    if (n > MAX_FORTUNES) {
      throw new AppException(n + " is too large");
    }
    var fortunes = new String[n];
    for (var i = 0; i < n; i++) {
      fortunes[i] = shellService.exec(FORTUNE, EXECUTE_TIMEOUT);
    }
    return fortunes;
  }
}
