package com.odroid.erizo.spring;

import static com.odroid.erizo.rest.RestConstants.BASE_URL;
import static com.odroid.erizo.rest.RestConstants.BASE_WEB_URL;
import static com.odroid.erizo.rest.RestConstants.ROOT_URL;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class SpaConfig implements WebMvcConfigurer {

  private static final String REDIRECT = "redirect:";
  private static final String MAIN_HTML = "index.html";

  @Override
  public void addViewControllers(ViewControllerRegistry registry) {
    registry.addViewController(ROOT_URL).setViewName(REDIRECT + BASE_URL);
    registry.addViewController(BASE_URL).setViewName(REDIRECT + BASE_WEB_URL);
  }

  @Override
  public void addResourceHandlers(ResourceHandlerRegistry registry) {
    registry.addResourceHandler(BASE_WEB_URL).addResourceLocations("classpath:/static" + BASE_WEB_URL)
        .resourceChain(true).addResolver(new SpaPathResourceResolver("/static" + BASE_WEB_URL + MAIN_HTML));
    registry.addResourceHandler(BASE_WEB_URL + "**").addResourceLocations("classpath:/static" + BASE_WEB_URL)
        .resourceChain(true).addResolver(new SpaPathResourceResolver("/static" + BASE_WEB_URL + MAIN_HTML));
  }

}
