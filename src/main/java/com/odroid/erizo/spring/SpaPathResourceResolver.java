package com.odroid.erizo.spring;

import java.io.IOException;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.web.servlet.resource.PathResourceResolver;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SpaPathResourceResolver extends PathResourceResolver {

  private String mainResourcePath;

  public SpaPathResourceResolver(String mainResourcePath) {
    this.mainResourcePath = mainResourcePath;
  }

  @Override
  protected Resource getResource(String resourcePath, Resource location) throws IOException {
    var requestedResource = location.createRelative(resourcePath);
    if (requestedResource.exists()) {
      log.debug("_Looking for {} at {} resolved as {}: found", resourcePath, location, requestedResource);
      return requestedResource;
    }
    log.debug("_Looking for {} at {} resolved as {}: not found, using {}", resourcePath, location, requestedResource,
        mainResourcePath);
    return new ClassPathResource(mainResourcePath);
  }

}
