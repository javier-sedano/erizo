package com.odroid.erizo;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class AppException extends RuntimeException {
  private static final long serialVersionUID = 1L;

  public AppException(String message) {
    super(message);
  }
}
