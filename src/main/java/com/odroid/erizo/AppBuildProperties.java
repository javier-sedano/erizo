package com.odroid.erizo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.info.BuildProperties;
import org.springframework.stereotype.Component;

@Component("appBuildProperties")
public class AppBuildProperties {

  private BuildProperties buildProperties;

  public AppBuildProperties(@Autowired(required = false) BuildProperties buildProperties) {
    this.buildProperties = buildProperties;
  }

  public String toString() {
    if (buildProperties == null) {
      return "dev";
    }
    return buildProperties.getVersion() + "@" + buildProperties.getTime();
  }
}
