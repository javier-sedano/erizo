package com.odroid.erizo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ErizoApplication {

	public static void main(String[] args) {
    SpringApplication.run(ErizoApplication.class, args);
	}

}
