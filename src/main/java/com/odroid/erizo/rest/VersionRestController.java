package com.odroid.erizo.rest;

import static com.odroid.erizo.rest.RestConstants.BASE_REST_URL;
import static org.springframework.http.MediaType.TEXT_PLAIN_VALUE;

import com.odroid.erizo.AppBuildProperties;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(BASE_REST_URL)
public class VersionRestController {

  private AppBuildProperties appBuildProperties;

  public VersionRestController(AppBuildProperties appBuildProperties) {
    this.appBuildProperties = appBuildProperties;
  }

  @GetMapping(value = "version", produces = { TEXT_PLAIN_VALUE })
  public String ver() {
    return appBuildProperties.toString();
  }
}
