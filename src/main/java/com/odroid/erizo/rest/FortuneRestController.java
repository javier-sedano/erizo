package com.odroid.erizo.rest;

import static com.odroid.erizo.rest.RestConstants.BASE_REST_URL;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import com.odroid.erizo.service.FortuneService;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(BASE_REST_URL)
public class FortuneRestController {

  private FortuneService fortuneService;

  public FortuneRestController(FortuneService fortuneService) {
    this.fortuneService = fortuneService;
  }

  @GetMapping(value = "fortune", produces = { APPLICATION_JSON_VALUE })
  public String[] fortune(@RequestParam(value = "n", defaultValue = "1") int n) {
    return fortuneService.getFortunes(n);
  }
}
