package com.odroid.erizo.integration;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.odroid.erizo.ErizoApplication;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;

@SpringBootTest(classes = ErizoApplication.class)
class ErizoApplicationIntegrationTest {

  @Autowired
  ApplicationContext applicationContext;

  @Test
  void contextLoads() {
    assertNotNull(applicationContext);
  }
}
