package com.odroid.erizo.integration.service;

import static com.odroid.erizo.TestConstants.MAX_FORTUNES;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.odroid.erizo.AppException;
import com.odroid.erizo.ErizoApplication;
import com.odroid.erizo.service.FortuneService;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = ErizoApplication.class)
class FortuneServiceIntegrationTest {

  @Autowired
  FortuneService fortuneService;

  @Test
  void returnsOneFortune() {
    String[] fortunes = fortuneService.getFortunes(1);
    assertEquals(1, fortunes.length);
    assertNotNull(fortunes[0]);
  }

  @Test
  void returnsSeveralFortunes() {
    String[] fortunes = fortuneService.getFortunes(MAX_FORTUNES);
    assertEquals(MAX_FORTUNES, fortunes.length);
    for (String fortune : fortunes) {
      assertNotNull(fortune);
    }
  }

  @Test
  void rejectsTooManyFortunes() {
    assertThrows(AppException.class, () -> {
      fortuneService.getFortunes(MAX_FORTUNES + 1);
    });
  }

  @Test
  void rejectsTooFewFortunes() {
    assertThrows(AppException.class, () -> {
      fortuneService.getFortunes(0);
    });
  }
}
