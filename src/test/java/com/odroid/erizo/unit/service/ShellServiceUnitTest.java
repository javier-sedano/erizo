package com.odroid.erizo.unit.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.odroid.erizo.service.ShellService;

import org.junit.jupiter.api.Test;

class ShellServiceUnitTest {
  @Test
  void worksOnExistingBinary() {
    ShellService shellService = new ShellService();
    String hello = "Hello";
    String result = shellService.exec("/bin/echo -n " + hello, 6000);
    assertEquals(hello, result);
  }

  @Test
  void detectsFailOnNonExistingBinary() {
    ShellService shellService = new ShellService();
    String hello = "Hello";
    assertThrows(IllegalArgumentException.class, () -> {
      shellService.exec("/bin/nonExisting -n " + hello, 6000);
    });
  }

}
