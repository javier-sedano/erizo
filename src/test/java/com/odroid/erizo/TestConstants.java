package com.odroid.erizo;

public abstract class TestConstants {
  public static final int MAX_FORTUNES = 5;

  private TestConstants() {
  }

}
