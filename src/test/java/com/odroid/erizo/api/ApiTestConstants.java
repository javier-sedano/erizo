package com.odroid.erizo.api;

public abstract class ApiTestConstants {
  public static final String ROOT_URL = "/";
  public static final String BASE_URL = ROOT_URL + "erizo/";
  public static final String BASE_REST_URL = BASE_URL + "rest/";
  public static final String BASE_WEB_URL = BASE_URL + "web/";

  private ApiTestConstants() {
  }
}
