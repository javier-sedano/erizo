package com.odroid.erizo.api;

import static com.odroid.erizo.api.ApiTestConstants.BASE_URL;
import static com.odroid.erizo.api.ApiTestConstants.BASE_WEB_URL;
import static com.odroid.erizo.api.ApiTestConstants.ROOT_URL;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.log;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import com.odroid.erizo.ErizoApplication;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.test.web.servlet.MockMvc;;

@SpringBootTest(classes = ErizoApplication.class, properties = {
    "logging.level.org.springframework.test.web.servlet.result=DEBUG" })
@AutoConfigureMockMvc
class SpaServingApiTests {

  private static final String LOCATION_HEADER = "Location";

  private static final String EXISTING_NON_WEB_RESOURCE = BASE_URL + "/existingResource.txt";
  private static final String NON_EXISTING_NON_WEB_RESOURCE = BASE_URL + "/nonExistingResource.txt";
  private static final String EXISTING_WEB_RESOURCE = BASE_WEB_URL + "/existingResource.txt";
  private static final String NON_EXISTING_WEB_RESOURCE = BASE_WEB_URL + "/nonExistingResource.txt";
  private static final String INDEX_HTML = BASE_WEB_URL + "/index.html";

  @Value("classpath:static" + INDEX_HTML)
  private Resource indexHtmlResource;

  @Value("classpath:static" + EXISTING_WEB_RESOURCE)
  private Resource existingWebResource;

  @Value("classpath:static" + EXISTING_NON_WEB_RESOURCE)
  private Resource existingNonWebResource;

  @Autowired
  private MockMvc mockMvc;

  @Test
  void redirectsRootToBase() throws Exception {
    mockMvc.perform(get(ROOT_URL)).andDo(log()).andExpect(status().is3xxRedirection())
        .andExpect(header().stringValues(LOCATION_HEADER, BASE_URL));
  }

  @Test
  void redirectsBaseToWeb() throws Exception {
    mockMvc.perform(get(BASE_URL)).andDo(log()).andExpect(status().is3xxRedirection())
        .andExpect(header().stringValues(LOCATION_HEADER, BASE_WEB_URL));
  }

  @Test
  void returnsExistingNonWebResource() throws Exception {
    returnsResourceForUrl(EXISTING_NON_WEB_RESOURCE, existingNonWebResource);
  }

  @Test
  void doesNotReturnNonExistingNonWebResource() throws Exception {
    mockMvc.perform(get(NON_EXISTING_NON_WEB_RESOURCE)).andDo(log()).andExpect(status().isNotFound());
  }

  @Test
  void returnsExistingWebResource() throws Exception {
    returnsResourceForUrl(EXISTING_WEB_RESOURCE, existingWebResource);
  }

  @Test
  void returnsIndexHtml() throws Exception {
    returnsResourceForUrl(INDEX_HTML, indexHtmlResource);
  }

  @Test
  void returnsIndexHtmlForWebRoot() throws Exception {
    returnsResourceForUrl(BASE_WEB_URL, indexHtmlResource);
  }

  @Test
  void returnsIndexHtmlForNonExistingWebResource() throws Exception {
    returnsResourceForUrl(NON_EXISTING_WEB_RESOURCE, indexHtmlResource);
  }

  private void returnsResourceForUrl(String url, Resource resource) throws Exception, IOException {
    mockMvc.perform(get(url)).andDo(log()).andExpect(status().isOk())
        .andExpect(content().string(readResource(resource)));
  }

  private String readResource(Resource resource) throws IOException {
    try (InputStream is = resource.getInputStream()) {
      return IOUtils.toString(is, StandardCharsets.UTF_8);
    }
  }
}
