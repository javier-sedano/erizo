package com.odroid.erizo.api;

import static com.odroid.erizo.TestConstants.MAX_FORTUNES;
import static com.odroid.erizo.api.ApiTestConstants.BASE_REST_URL;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.log;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.odroid.erizo.ErizoApplication;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;;

@SpringBootTest(classes = ErizoApplication.class, properties = {
    "logging.level.org.springframework.test.web.servlet.result=DEBUG" })
@AutoConfigureMockMvc
class FortuneApiTests {

  private static final String FORTUNE_URL = BASE_REST_URL + "fortune";

  @Autowired
  private MockMvc mockMvc;

  private ObjectMapper jackson = new ObjectMapper();

  @Test
  void getsOneFortune() throws Exception {
    MvcResult mvcResult = mockMvc.perform(get(FORTUNE_URL)).andDo(log()).andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(APPLICATION_JSON_VALUE)).andReturn();
    String[] fortunes = jackson.readValue(mvcResult.getResponse().getContentAsString(), new TypeReference<String[]>() {
    });
    assertEquals(1, fortunes.length);
    assertNotNull(fortunes[0]);
  }

  @Test
  void getsSeveralFortunes() throws Exception {
    MvcResult mvcResult = mockMvc.perform(get(FORTUNE_URL).param("n", "" + MAX_FORTUNES)).andDo(log())
        .andExpect(status().isOk()).andExpect(content().contentTypeCompatibleWith(APPLICATION_JSON_VALUE)).andReturn();
    String[] fortunes = jackson.readValue(mvcResult.getResponse().getContentAsString(), new TypeReference<String[]>() {
    });
    assertEquals(MAX_FORTUNES, fortunes.length);
    for (String fortune : fortunes) {
      assertNotNull(fortune);
    }
  }

  @Test
  void doesNotGetTooManyFortunes() throws Exception {
    mockMvc.perform(get(FORTUNE_URL).param("n", "" + (MAX_FORTUNES + 1))).andDo(log()).andExpect(status().isBadRequest());
  }

  @Test
  void doesNotGetTooFewFortunes() throws Exception {
    mockMvc.perform(get(FORTUNE_URL).param("n", "0")).andDo(log()).andExpect(status().isBadRequest());
  }
}
