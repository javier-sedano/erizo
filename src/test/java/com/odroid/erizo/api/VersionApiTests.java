package com.odroid.erizo.api;

import static com.odroid.erizo.api.ApiTestConstants.BASE_REST_URL;
import static org.springframework.http.MediaType.TEXT_PLAIN_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.log;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.odroid.erizo.ErizoApplication;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;;

@SpringBootTest(classes = ErizoApplication.class,
properties = {
  "logging.level.org.springframework.test.web.servlet.result=DEBUG"
})
@AutoConfigureMockMvc
class VersionApiTests {

  private static final String VERSION_URL = BASE_REST_URL + "version";

  @Autowired
  private MockMvc mockMvc;

  @Test
  void getsVersion() throws Exception {
    mockMvc.perform(get(VERSION_URL)).andDo(log()).andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(TEXT_PLAIN_VALUE));
  }
}
