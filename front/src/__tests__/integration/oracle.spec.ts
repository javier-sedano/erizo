import { describe, it, expect, beforeEach, afterEach, vi } from "vitest";
import { mount, flushPromises, VueWrapper } from "@vue/test-utils";
import { setActivePinia, createPinia } from "pinia";
import { mockAxios } from "../tests.utils";
import Oracle from "../../views/Oracle.vue";
import { useErrorStore } from "../../stores/error.store";

const { axiosGetMock } = mockAxios();

const GIPHY_URL = "https://api.giphy.com/v1/gifs/random";
const MNIMAL_LISTEN_TIME = 1000;
const MAX_RELATION_FACTOR = 3;

beforeEach(() => {
  setActivePinia(createPinia());
});

afterEach(() => {
  vi.clearAllMocks();
});

describe("Oracle view", async () => {
  function expectIdle(wrapper: VueWrapper) {
    expect(wrapper.find('[data-test-id~="microphone"]').exists()).toEqual(true);
    expect(wrapper.find('[data-test-id~="spinner"]').exists()).toEqual(false);
    expect(wrapper.find('[data-test-id~="answerContainer"]').exists()).toEqual(false);
  }

  function expectListening(wrapper: VueWrapper) {
    expect(wrapper.find('[data-test-id~="microphone"]').exists()).toEqual(true);
    expect(wrapper.find('[data-test-id~="spinner"]').exists()).toEqual(true);
    expect(wrapper.find('[data-test-id~="answerContainer"]').exists()).toEqual(false);
  }

  function expectLoading(wrapper: VueWrapper) {
    expect(wrapper.find('[data-test-id~="microphone"]').exists()).toEqual(false);
    expect(wrapper.find('[data-test-id~="spinner"]').exists()).toEqual(false);
    expect(wrapper.find('[data-test-id~="answerContainer"]').exists()).toEqual(false);
  }

  function expectLoaded(wrapper: VueWrapper, gifUrl: string, giphyUrl: string, author: string) {
    expect(wrapper.find('[data-test-id~="microphone"]').exists()).toEqual(false);
    expect(wrapper.find('[data-test-id~="spinner"]').exists()).toEqual(false);
    expect(wrapper.find('[data-test-id~="answerContainer"]').exists()).toEqual(true);
    expect(wrapper.find('[data-test-id~="answerContainer"]').attributes("style")).toEqual(
      `background-image: url(${gifUrl});`,
    );
    if (author) {
      expect(wrapper.find('[data-test-id~="answerAuthor"]').text()).toEqual(author);
    }
    expect(wrapper.find('[data-test-id~="answerGiphyUrl"]').attributes("href")).toEqual(giphyUrl);
  }

  function mockNow(now) {
    vi.spyOn(global.Date, "now").mockImplementation(() => now);
  }

  function mockGiphy(response: any) {
    axiosGetMock.mockImplementationOnce(() => Promise.resolve({ data: response }));
  }

  function createWrapper(): VueWrapper {
    const wrapper = mount(Oracle, {
      global: {
        stubs: {
          "font-awesome-icon": { template: "<span font-awesome-icon/>" },
          "font-awesome-layers": { template: "<span font-awesome-layers><slot/></span>" },
        },
      },
      attachTo: document.body,
    });
    return wrapper;
  }
  it("Should create idle", () => {
    const wrapper = createWrapper();
    expectIdle(wrapper);
    expect(wrapper.element).toMatchSnapshot("Initial");
  });
  it("Should not load anything if called too quickly", async () => {
    const wrapper = createWrapper();
    const start = Date.now();
    wrapper.find('[data-test-id~="button"]').trigger("mousedown");
    await flushPromises();
    expectListening(wrapper);
    mockNow(start + MNIMAL_LISTEN_TIME - 1);
    wrapper.find('[data-test-id~="button"]').trigger("mouseup");
    await flushPromises();
    expectIdle(wrapper);
    expect(axiosGetMock).not.toHaveBeenCalled();
  });
  async function testLoadWithEvent(
    startEvent: string,
    stopEvent: string,
    aGifAnswer: any,
  ): Promise<VueWrapper> {
    global.Math.random = () => 0;
    const wrapper = createWrapper();
    const start = Date.now();
    await wrapper.find('[data-test-id~="button"]').trigger(startEvent);
    expectListening(wrapper);
    mockNow(start + MNIMAL_LISTEN_TIME + 1);
    mockGiphy(aGifAnswer);
    await wrapper.find('[data-test-id~="button"]').trigger(stopEvent);
    expectLoaded(
      wrapper,
      aGifAnswer.data.images.original.url,
      aGifAnswer.data.url,
      aGifAnswer.data.user?.display_name,
    );
    expect(wrapper.element).toMatchSnapshot("Loaded");
    expect(axiosGetMock).toHaveBeenCalledWith(GIPHY_URL, {
      params: { api_key: expect.stringMatching(/.*/), limit: 1, tag: "Yes" },
    });
    await wrapper.find('[data-test-id~="answerContainer"]').trigger("click");
    expectIdle(wrapper);
    return wrapper;
  }
  it("Should load an answer if called slowly and back to idle (with mouse)", async () => {
    const aGifAnswer = {
      data: {
        images: {
          original: {
            height: 10,
            width: 10 * (MAX_RELATION_FACTOR * 0.9),
            url: "https://a/url.gif",
          },
        },
        url: "https://giphy/url",
        user: {
          display_name: "qwerty",
        },
      },
    };
    const wrapper = await testLoadWithEvent("mousedown", "mouseup", aGifAnswer);
    expect(wrapper.element).toMatchSnapshot("Idle");
  });
  it("Should load an answer if called slowly and back to idle (with touch)", async () => {
    const aGifAnswer = {
      data: {
        images: {
          original: {
            height: 10,
            width: 10 * (MAX_RELATION_FACTOR * 0.9),
            url: "https://a/url.gif",
          },
        },
        url: "https://giphy/url",
        user: {
          display_name: "qwerty",
        },
      },
    };
    const wrapper = await testLoadWithEvent("touchstart", "touchend", aGifAnswer);
    expect(wrapper.element).toMatchSnapshot("Idle");
  });
  it("Should load an answer if called slowly and back to idle (without author)", async () => {
    const aGifAnswerWithoutAuthor = {
      data: {
        images: {
          original: {
            height: 10,
            width: 10 * (MAX_RELATION_FACTOR * 0.9),
            url: "https://a/url.gif",
          },
        },
        url: "https://giphy/url",
      },
    };
    const wrapper = await testLoadWithEvent("mousedown", "mouseup", aGifAnswerWithoutAuthor);
    expect(wrapper.element).toMatchSnapshot("Idle");
  });
  it("Should detect error", async () => {
    const anError = "An error";
    const wrapper = createWrapper();
    const start = Date.now();
    await wrapper.find('[data-test-id~="button"]').trigger("mousedown");
    expectListening(wrapper);
    mockNow(start + MNIMAL_LISTEN_TIME + 1);
    axiosGetMock.mockImplementationOnce(() => Promise.reject(new Error(anError)));
    await wrapper.find('[data-test-id~="button"]').trigger("mouseup");
    expect(axiosGetMock).toHaveBeenCalledWith(GIPHY_URL, {
      params: { api_key: expect.stringMatching(/.*/), limit: 1, tag: expect.anything() },
    });
    expect(useErrorStore().errorMessage).toEqual(`Error: ${anError}`);
    expectIdle(wrapper);
    expect(wrapper.element).toMatchSnapshot("With error");
  });
});
