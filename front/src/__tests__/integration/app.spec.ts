import { describe, it, expect, beforeEach, afterEach, vi } from "vitest";
import { mount, flushPromises, VueWrapper } from "@vue/test-utils";
import { setActivePinia, createPinia } from "pinia";
import { REST_BASE_URL, mockAxios } from "../tests.utils";
import App from "../../App.vue";
import { useErrorStore } from "../../stores/error.store";

const { axiosGetMock } = mockAxios();

export const VERSION = "42";

beforeEach(() => {
  setActivePinia(createPinia());
});

afterEach(() => {
  vi.clearAllMocks();
});

describe("App container", async () => {
  async function createWrapper(): Promise<VueWrapper> {
    axiosGetMock.mockImplementationOnce(() => Promise.resolve({ data: VERSION }));
    const wrapper = mount(App, {
      global: {
        stubs: {
          "router-link": { template: "<span router-link><slot/></span>" },
          "router-view": { template: "<span router-view/>" },
          "font-awesome-icon": { template: "<span font-awesome-icon/>" },
        },
      },
      attachTo: document.body,
    });
    expect(wrapper.element).toMatchSnapshot("Initial");
    expect(wrapper.find('[data-test-id~="version"]').text()).toEqual("Version ...");
    await flushPromises();
    expect(wrapper.find('[data-test-id~="version"]').text()).toEqual(`Version ${VERSION}`);
    expect(axiosGetMock).toHaveBeenCalledWith(`${REST_BASE_URL}/version`);
    axiosGetMock.mockClear();
    expect(wrapper.element).toMatchSnapshot("Initial with version");
    return wrapper;
  }
  it("Should create with version", async () => {
    await createWrapper();
  });
  it("Should toggle menu", async () => {
    const wrapper = await createWrapper();
    const burgerButton = wrapper.find('[data-test-id~="burgerButton"]');
    const smallMenu = wrapper.find('[data-test-id~="smallMenu"]');
    expect(smallMenu.isVisible()).toEqual(false);
    expect(wrapper.element).toMatchSnapshot("Before toggle menu");
    await burgerButton.trigger("click");
    expect(smallMenu.isVisible()).toEqual(true);
    expect(wrapper.element).toMatchSnapshot("After show menu");
    await smallMenu.trigger("click");
    expect(smallMenu.isVisible()).toEqual(false);
    expect(wrapper.element).toMatchSnapshot("After hide menu");
    await burgerButton.trigger("click");
    expect(smallMenu.isVisible()).toEqual(true);
    expect(wrapper.element).toMatchSnapshot("After show menu");
    await burgerButton.trigger("click");
    expect(smallMenu.isVisible()).toEqual(false);
    expect(wrapper.element).toMatchSnapshot("After hide menu");
  });
  it("Should show error when stored and dismiss it", async () => {
    const AN_ERROR = "An error";
    const errorStore = useErrorStore();
    const wrapper = await createWrapper();
    expect(wrapper.find('[data-test-id~="errorDialog"]').exists()).toEqual(false);
    expect(wrapper.element).toMatchSnapshot("Initial");
    errorStore.setErrorMessage(AN_ERROR);
    await flushPromises();
    expect(wrapper.find('[data-test-id~="errorDialog"]').exists()).toEqual(true);
    expect(wrapper.find('[data-test-id~="errorMessage"]').text()).toEqual(AN_ERROR);
    expect(wrapper.element).toMatchSnapshot("Showing");
    await wrapper.find('[data-test-id~="okButton"]').trigger("click");
    expect(wrapper.find('[data-test-id~="errorDialog"]').exists()).toEqual(false);
    expect(wrapper.element).toMatchSnapshot("Initial");
  });
});
