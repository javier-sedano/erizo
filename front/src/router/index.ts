import { createRouter, createWebHistory } from "vue-router";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "Root",
      redirect: "/fortune",
    },
    {
      path: "/fortune",
      name: "Fortune",
      component: () => import("../views/Fortune.vue"),
    },
    {
      path: "/oracle",
      name: "Oracle",
      component: () => import("../views/Oracle.vue"),
    },
    {
      path: "/:pathMatch(.*)*",
      name: "NotFound",
      component: () => import("../views/NotFound.vue"),
    },
  ],
});

export default router;
