#!/bin/bash
cd "$(dirname "$0")"
TAG=latest
if [ "$1" ]
then
  TAG="$1"
fi
REGISTRY=registry.gitlab.com/javier-sedano/erizo/app
IMAGE=$REGISTRY:$TAG
containerId=`docker ps --filter "name=erizo" --filter "status=running" --quiet`
docker container stop erizo
docker container rm erizo
docker image ls | grep $REGISTRY | while read LINE
do
  OLD_TAG=`echo $LINE | cut -d\  -f2`
  docker image rm $REGISTRY:$OLD_TAG
done
docker pull $IMAGE
docker container create -m 70m -p 5680:8080 --restart unless-stopped --name erizo $IMAGE
if [ "$containerId" ]
then
  docker container start erizo
fi
