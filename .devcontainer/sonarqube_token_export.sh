#!/bin/bash
pushd `dirname $0` > /dev/null

TOKEN=$(./sonarqube_token.sh)

echo "sonarToken=${TOKEN}" > ../.sonar_token.properties

popd > /dev/null
